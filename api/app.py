from flask import Flask, Response
import logging

app = Flask(__name__)


@app.route('/call-me-celery')
def call_me_celery() -> str:
    logging.info("You called the api with a celery task baby !")
    response = Response(status=200,
                        response="You called the api with a celery task baby !")
    return response

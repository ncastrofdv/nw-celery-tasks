import json
import os

import requests
from celery import Celery
from celery.utils.log import get_task_logger

CELERY_BROKER_URL = os.environ.get('CELERY_BROKER_URL'),
CELERY_RESULT_BACKEND = os.environ.get('CELERY_RESULT_BACKEND')

celery = Celery(
    'tasks', broker=CELERY_BROKER_URL
)

celery.conf.beat_schedule = {
    # Executes every Monday morning at 7:30 a.m.
    'add-every-monday-morning': {
        'task': 'tasks.dumb_task',
        'schedule': 30.0,
    },
}

logger = get_task_logger(__name__)


@celery.task(name='tasks.dumb_task')
def dumb_task() -> int:
    logger.info("Hello, I'll come back in a minute")
    response = requests.get("http://web:7111/call-me-celery")

    return 200
